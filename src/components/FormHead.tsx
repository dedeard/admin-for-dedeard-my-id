import { useState } from 'react'
import { DatabaseReference, set } from 'firebase/database'
import { Form, Button, FormGroup, Input, Label } from 'reactstrap'
import { IDataHead } from '../firebase/types'
import Swal from '../utils/swal'

type PropsType = {
  head?: IDataHead
  headRef: DatabaseReference
}

export default function FormHead({ head, headRef }: PropsType) {
  const [input, setInput] = useState<IDataHead>(head || { title: '', description: '' })
  const [loading, setLoading] = useState(false)

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value, name } = e.currentTarget
    setInput({ ...input, [name]: value })
  }

  const onSubumit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    setLoading(true)
    try {
      await set(headRef, input)
      Swal.fire({
        title: 'Head Successfully updated',
        icon: 'success',
      })
    } catch (e: any) {
      Swal.fire({
        title: 'Failed to upload head!',
        text: e.message,
        icon: 'error',
      })
    }
    setLoading(false)
  }

  return (
    <div className="card mb-3">
      <div className="card-header">Head</div>
      <div className="card-body">
        <Form onSubmit={onSubumit}>
          <FormGroup>
            <Label for="title">Title</Label>
            <Input id="title" name="title" value={input.title} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Label for="description">Description</Label>
            <Input id="description" name="description" type="textarea" value={input.description} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Button type="submit" color="primary" disabled={loading}>
              Submit
            </Button>
          </FormGroup>
        </Form>
      </div>
    </div>
  )
}
