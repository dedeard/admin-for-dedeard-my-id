import { useState } from 'react'
import { Collapse, Navbar, NavbarToggler, Nav, NavItem, Button } from 'reactstrap'
import { Link } from 'react-router-dom'
import { User, getAuth, signOut } from 'firebase/auth'

const AuthNavs = () => (
  <>
    <NavItem>
      <Link to="/" className="nav-link">
        Home
      </Link>
    </NavItem>
    <NavItem>
      <Link to="/about" className="nav-link">
        About
      </Link>
    </NavItem>
    <NavItem>
      <Link to="/gallery" className="nav-link">
        Gallery
      </Link>
    </NavItem>
    <NavItem>
      <Link to="/contact" className="nav-link">
        Contact
      </Link>
    </NavItem>
    <NavItem>
      <Link to="/config" className="nav-link">
        Config
      </Link>
    </NavItem>
  </>
)

const GuestNavs = () => (
  <>
    <NavItem>
      <Link to="/auth" className="nav-link">
        Login
      </Link>
    </NavItem>
  </>
)

const AppNavbar = ({ user }: { user?: User | null }) => {
  const [isOpen, setIsOpen] = useState(false)
  const toggle = () => setIsOpen(!isOpen)
  return (
    <Navbar expand="lg" light className="shadow bg-white" container>
      <Link to="/" className="navbar-brand">
        DEDEARD
      </Link>
      <NavbarToggler onClick={toggle} />
      <Collapse isOpen={isOpen} navbar>
        <Nav navbar>{user ? <AuthNavs /> : <GuestNavs />}</Nav>
        {user && (
          <Button color="danger" onClick={() => signOut(getAuth())} className="ms-auto">
            Log Out
          </Button>
        )}
      </Collapse>
    </Navbar>
  )
}

export default AppNavbar
