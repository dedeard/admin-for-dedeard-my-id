import { ref, getDatabase } from 'firebase/database'
import { useObjectVal } from 'react-firebase-hooks/database'
import ImageUploader from './ImageUploader'
import { IGalleryPage } from '../../firebase/types'
import ImageList from './ImageList'
import React from 'react'
import FormHead from '../../components/FormHead'
import FormBody from './FormBody'

const db = getDatabase()
const headRef = ref(db, 'gallery/head')

export default function GalleryPage() {
  const [value, loading] = useObjectVal<IGalleryPage>(ref(db, 'gallery'))
  const images = value?.body?.images || {}
  if (loading) return <></>
  return (
    <div className="container py-4">
      <FormHead headRef={headRef} head={value?.head} />
      <FormBody data={value} />
      <ImageUploader />
      <div className="row py-3">
        {Object.keys(images).map((_id) => (
          <React.Fragment key={_id}>{<ImageList _id={_id} image={images[_id]} />}</React.Fragment>
        ))}
      </div>
    </div>
  )
}
