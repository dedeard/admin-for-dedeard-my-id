import { useRef, useState } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Spinner } from 'reactstrap'
import { FileUploader } from 'react-drag-drop-files'
import Cropper from 'react-cropper'
import p from 'pica'
import { getDatabase, ref as dRef, get as dGet, push, serverTimestamp } from 'firebase/database'
import { randomStr } from '../../utils/randomStr'
import Swal from '../../utils/swal'

const db = getDatabase()
const configRef = dRef(db, 'config')
const pica = p()

export default function ImageUploader() {
  const cropperRef = useRef<HTMLImageElement>(null)
  const [src, setSrc] = useState('')
  const [modal, setModal] = useState(false)
  const [cropped, setCropped] = useState<HTMLCanvasElement>()
  const [loading, setLoading] = useState(false)

  const onCrop = () => {
    const imageElement: any = cropperRef?.current
    const cropper: any = imageElement?.cropper
    setCropped(cropper.getCroppedCanvas())
  }

  const handleChange = async (file: File) => {
    if (file && !modal) {
      setSrc(URL.createObjectURL(file))
      toggle()
    }
  }

  const toggle = () => {
    if (modal) {
      setModal(false)
      setSrc('')
    } else {
      setModal(true)
    }
  }

  const upload = async () => {
    setModal(false)
    setLoading(true)
    if (cropped) {
      const cnvs = document.createElement('canvas')

      cnvs.width = 1280
      cnvs.height = 960

      const blob = await pica.resize(cropped, cnvs).then((result) => pica.toBlob(result, 'image/jpeg', 1))

      const apiKey = (await dGet(configRef)).val().imgbb_key

      const alt = randomStr()
      const fileName = alt + '.jpeg'
      const formData = new FormData()
      formData.append('image', blob)
      formData.append('key', apiKey)
      formData.append('name', fileName)

      const { data } = await fetch('https://api.imgbb.com/1/upload', { method: 'POST', body: formData }).then((res) => res.json())

      try {
        const imagesRef = dRef(db, 'gallery/body/images')
        await push(imagesRef, {
          fileName,
          alt,
          sm_url: data.medium.url,
          url: data.url,
          createdAt: serverTimestamp(),
          imgbb: {
            id: data.id,
            size: data.size,
            delete_url: data.delete_url,
            image_url: data.image.url,
            medium_url: data.medium.url,
            thumb_url: data.thumb.url,
          },
        })
        Swal.fire({
          title: 'Image Successfully uploaded',
          icon: 'success',
        })
      } catch (e: any) {
        Swal.fire({
          title: 'Failed to upload image!',
          text: e.message,
          icon: 'error',
        })
      }
      setLoading(false)
    }
  }

  return (
    <>
      <div className="card">
        <div className="card-header">Upload Image</div>
        <div className="card-body position-relative">
          <Modal isOpen={modal} toggle={toggle} size="lg">
            <ModalHeader toggle={toggle}>Modal title</ModalHeader>
            <ModalBody className="p-0">
              {src && <Cropper src={src} style={{ width: '100%', height: 450 }} aspectRatio={4 / 3} crop={onCrop} ref={cropperRef} />}
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={upload}>
                Upload
              </Button>
              <Button color="secondary" onClick={toggle}>
                Cancel
              </Button>
            </ModalFooter>
          </Modal>
          {loading && (
            <div
              className="position-absolute d-flex justify-content-center align-items-center w-100 h-100 top-0 start-0"
              style={{ backgroundColor: 'rgba(255,255,255, 0.8)', zIndex: 5 }}
            >
              <Spinner color="primary">Loading...</Spinner>
            </div>
          )}
          <div className="d-flex">
            <div className="mx-auto">
              <FileUploader handleChange={handleChange} name="file" />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
