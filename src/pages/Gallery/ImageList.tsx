import { useState } from 'react'
import { IDataImage } from '../../firebase/types'
import { Input, InputGroup, InputGroupText, Button } from 'reactstrap'
import { getStorage, ref as sRef, deleteObject } from 'firebase/storage'
import { getDatabase, ref as dRef, update, remove } from 'firebase/database'
import Swal from '../../utils/swal'

const db = getDatabase()
const storage = getStorage()

const ImageList = ({ _id, image }: { _id: string; image: IDataImage }) => {
  const [alt, setAlt] = useState(image.alt)
  const [saveLoading, setSaveLoading] = useState(false)
  const [destroyLoading, setDestroyLoading] = useState(false)

  const save = async () => {
    setSaveLoading(true)
    const ref = dRef(db, 'gallery/body/images/' + _id)
    try {
      await update(ref, { alt })
      Swal.fire({
        title: 'Image ALT Successfully updated!',
        icon: 'success',
      })
    } catch (e: any) {
      Swal.fire({
        title: 'Failed to update Image ALT!',
        text: e.message,
        icon: 'error',
      })
    }
    setSaveLoading(false)
  }

  const destroy = async () => {
    setDestroyLoading(true)
    const { isConfirmed } = await Swal.fire({
      title: 'Do you want to delete this image?',
      icon: 'question',
      showDenyButton: true,
      denyButtonColor: '#0d6efd',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Yes, delete it',
    })
    if (isConfirmed) {
      try {
        if (image.imgbb) {
          await remove(dRef(db, 'gallery/body/images/' + _id))
          window.open(image.imgbb.delete_url, '_blank')
        } else {
          await remove(dRef(db, 'gallery/body/images/' + _id))
          if (image.fileName) {
            await deleteObject(sRef(storage, 'next/gallery/sm/' + image.fileName))
            await deleteObject(sRef(storage, 'next/gallery/lg/' + image.fileName))
          }
        }
        Swal.fire({
          title: 'Image Successfully deleted!',
          icon: 'success',
        })
      } catch (e: any) {
        Swal.fire({
          title: 'Failed to delete Image!',
          text: e.message,
          icon: 'error',
        })
      }
    }
    setDestroyLoading(false)
  }

  return (
    <div className="col-md-6 my-3">
      <div className="card">
        <img src={image.sm_url} className="card-img-top" alt={image.alt} />
        <div className="card-body">
          <InputGroup size="sm">
            <InputGroupText>ALT</InputGroupText>
            <Input placeholder="Image ALT" value={alt} onChange={(e) => setAlt(e.currentTarget.value)} />
            {alt !== image.alt && (
              <Button color="primary" onClick={save} disabled={saveLoading}>
                Save
              </Button>
            )}
            <Button color="danger" className="ms-auto" onClick={destroy} disabled={destroyLoading}>
              Delete
            </Button>
          </InputGroup>
        </div>
      </div>
    </div>
  )
}

export default ImageList
