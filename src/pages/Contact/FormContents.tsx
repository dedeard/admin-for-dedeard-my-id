import { useState } from 'react'
import { ref, getDatabase, set } from 'firebase/database'
import { Form, Button, FormGroup, Input, Label } from 'reactstrap'
import { IContactPage } from '../../firebase/types'
import Swal from '../../utils/swal'

const db = getDatabase()
export default function FormContents({ data }: { data?: IContactPage }) {
  const [input, setInput] = useState({
    address_title: data?.body?.contents?.address.title || '',
    address_text: data?.body?.contents?.address.text || '',
    phone_title: data?.body?.contents?.phone.title || '',
    phone_text: data?.body?.contents?.phone.text || '',
    email_title: data?.body?.contents?.email.title || '',
    email_text: data?.body?.contents?.email.text || '',
  })
  const [loading, setLoading] = useState(false)

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value, name } = e.currentTarget
    setInput({ ...input, [name]: value })
  }

  const onSubumit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    setLoading(true)
    try {
      await set(ref(db, 'contact/body/contents'), {
        address: {
          title: input.address_title,
          text: input.address_text,
        },
        phone: {
          title: input.phone_title,
          text: input.phone_text,
        },
        email: {
          title: input.email_title,
          text: input.email_text,
        },
      })
      Swal.fire({
        title: 'Contents Successfully updated',
        icon: 'success',
      })
    } catch (e: any) {
      Swal.fire({
        title: 'Failed to update Contents!',
        text: e.message,
        icon: 'error',
      })
    }
    setLoading(false)
  }

  return (
    <div className="card mb-3">
      <div className="card-header">Contents</div>
      <div className="card-body">
        <Form onSubmit={onSubumit}>
          <h4>ADDRESS</h4>
          <FormGroup>
            <Label for="address_title">Title</Label>
            <Input id="address_title" name="address_title" value={input.address_title} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Label for="address_text">Text</Label>
            <Input id="address_text" name="address_text" value={input.address_text} onChange={onChange} />
          </FormGroup>

          <h4>PHONE</h4>
          <FormGroup>
            <Label for="phone_title">Title</Label>
            <Input id="phone_title" name="phone_title" value={input.phone_title} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Label for="phone_text">Text</Label>
            <Input id="phone_text" name="phone_text" value={input.phone_text} onChange={onChange} />
          </FormGroup>

          <h4>EMAIL</h4>
          <FormGroup>
            <Label for="email_title">Title</Label>
            <Input id="email_title" name="email_title" value={input.email_title} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Label for="email_text">Text</Label>
            <Input id="email_text" name="email_text" value={input.email_text} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Button type="submit" color="primary" disabled={loading}>
              Submit
            </Button>
          </FormGroup>
        </Form>
      </div>
    </div>
  )
}
