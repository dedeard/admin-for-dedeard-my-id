import { useState } from 'react'
import { ref, getDatabase, update } from 'firebase/database'
import { Form, Button, FormGroup, Input, Label } from 'reactstrap'
import { IContactPage } from '../../firebase/types'
import Swal from '../../utils/swal'

const db = getDatabase()
export default function FormBody({ data }: { data?: IContactPage }) {
  const [input, setInput] = useState({
    title: data?.body?.title || '',
    formspree_key: data?.body?.formspree_key || '',
  })
  const [loading, setLoading] = useState(false)

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value, name } = e.currentTarget
    setInput({ ...input, [name]: value })
  }

  const onSubumit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    setLoading(true)
    try {
      await update(ref(db, 'contact/body'), input)
      Swal.fire({
        title: 'Body Successfully updated',
        icon: 'success',
      })
    } catch (e: any) {
      Swal.fire({
        title: 'Failed to update body!',
        text: e.message,
        icon: 'error',
      })
    }
    setLoading(false)
  }

  return (
    <div className="card mb-3">
      <div className="card-header">Body</div>
      <div className="card-body">
        <Form onSubmit={onSubumit}>
          <FormGroup>
            <Label for="title">Title</Label>
            <Input id="title" name="title" value={input.title} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Label for="formspree_key">
              <a href="https://formspree.io/" target="_blank">
                FormSpree
              </a>{' '}
              key
            </Label>
            <Input id="formspree_key" name="formspree_key" value={input.formspree_key} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Button type="submit" color="primary" disabled={loading}>
              Submit
            </Button>
          </FormGroup>
        </Form>
      </div>
    </div>
  )
}
