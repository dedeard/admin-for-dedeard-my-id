import { ref, getDatabase } from 'firebase/database'
import { useObjectVal } from 'react-firebase-hooks/database'
import { IContactPage } from '../../firebase/types'
import FormHead from '../../components/FormHead'
import FormBody from './FormBody'
import FormContents from './FormContents'

const db = getDatabase()
const headRef = ref(db, 'contact/head')

export default function ContactPage() {
  const [value, loading] = useObjectVal<IContactPage>(ref(db, 'contact'))
  if (loading) return <></>
  return (
    <div className="container py-4">
      <FormHead headRef={headRef} head={value?.head} />
      <FormBody data={value} />
      <FormContents data={value} />
    </div>
  )
}
