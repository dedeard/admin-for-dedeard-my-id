import { useState } from 'react'
import { ref, getDatabase, set } from 'firebase/database'
import { Form, Button, FormGroup, Input, Label } from 'reactstrap'
import { IHomePage } from '../../firebase/types'
import Swal from '../../utils/swal'

const db = getDatabase()
export default function FormContents({ data }: { data?: IHomePage }) {
  const [input, setInput] = useState({
    P1: data?.body?.contents?.P1 || '',
    P2: data?.body?.contents?.P2 || '',
    P3: data?.body?.contents?.P3 || '',
  })
  const [loading, setLoading] = useState(false)

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value, name } = e.currentTarget
    setInput({ ...input, [name]: value })
  }

  const onSubumit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    setLoading(true)
    try {
      await set(ref(db, 'home/body/contents'), input)
      Swal.fire({
        title: 'Contents Successfully updated',
        icon: 'success',
      })
    } catch (e: any) {
      Swal.fire({
        title: 'Failed to update Contents!',
        text: e.message,
        icon: 'error',
      })
    }
    setLoading(false)
  }

  return (
    <div className="card mb-3">
      <div className="card-header">Contents</div>
      <div className="card-body">
        <Form onSubmit={onSubumit}>
          <FormGroup>
            <Label for="P1">Line 1</Label>
            <Input id="P1" name="P1" value={input.P1} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Label for="P2">Line 2</Label>
            <Input id="P2" name="P2" value={input.P2} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Label for="P3">Line 3</Label>
            <Input id="P3" name="P3" value={input.P3} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Button type="submit" color="primary" disabled={loading}>
              Submit
            </Button>
          </FormGroup>
        </Form>
      </div>
    </div>
  )
}
