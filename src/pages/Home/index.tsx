import { ref, getDatabase } from 'firebase/database'
import { useObjectVal } from 'react-firebase-hooks/database'
import { IHomePage } from '../../firebase/types'
import FormHead from '../../components/FormHead'
import FormSocials from './FormSocials'
import FormContents from './FormContents'

const db = getDatabase()
const headRef = ref(db, 'home/head')

export default function HomePage() {
  const [value, loading] = useObjectVal<IHomePage>(ref(db, 'home'))
  if (loading) return <></>
  return (
    <div className="container py-4">
      <FormHead headRef={headRef} head={value?.head} />
      <FormContents data={value} />
      <FormSocials data={value} />
    </div>
  )
}
