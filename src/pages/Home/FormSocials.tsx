import { useState } from 'react'
import { ref, getDatabase, set } from 'firebase/database'
import { Form, Button, FormGroup, Input, Label } from 'reactstrap'
import { IHomePage } from '../../firebase/types'
import Swal from '../../utils/swal'

const db = getDatabase()
export default function FormSocials({ data }: { data?: IHomePage }) {
  const [input, setInput] = useState({
    IG: data?.body?.socials?.IG || '',
    GH: data?.body?.socials?.GH || '',
    WA: data?.body?.socials?.WA || '',
  })
  const [loading, setLoading] = useState(false)

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value, name } = e.currentTarget
    setInput({ ...input, [name]: value })
  }

  const onSubumit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    setLoading(true)
    try {
      await set(ref(db, 'home/body/socials'), input)
      Swal.fire({
        title: 'Socials Successfully updated',
        icon: 'success',
      })
    } catch (e: any) {
      Swal.fire({
        title: 'Failed to update Socials!',
        text: e.message,
        icon: 'error',
      })
    }
    setLoading(false)
  }

  return (
    <div className="card mb-3">
      <div className="card-header">Socials</div>
      <div className="card-body">
        <Form onSubmit={onSubumit}>
          <FormGroup>
            <Label for="IG">Instagram</Label>
            <Input id="IG" name="IG" value={input.IG} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Label for="GH">Github</Label>
            <Input id="GH" name="GH" value={input.GH} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Label for="WA">Whatsapp</Label>
            <Input id="WA" name="WA" value={input.WA} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Button type="submit" color="primary" disabled={loading}>
              Submit
            </Button>
          </FormGroup>
        </Form>
      </div>
    </div>
  )
}
