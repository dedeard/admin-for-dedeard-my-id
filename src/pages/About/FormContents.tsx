import { useState } from 'react'
import { ref, getDatabase, set } from 'firebase/database'
import { Form, Button, FormGroup, Input, Label } from 'reactstrap'
import { IAboutPage } from '../../firebase/types'
import Swal from '../../utils/swal'

const db = getDatabase()
export default function FormContents({ data }: { data?: IAboutPage }) {
  const [input, setInput] = useState({
    idn_title: data?.body?.contents?.idn?.title || '',
    idn_text: data?.body?.contents?.idn?.text || '',
    eng_title: data?.body?.contents?.eng?.title || '',
    eng_text: data?.body?.contents?.eng?.text || '',
    tech_title: data?.body?.contents?.tech?.title || '',
    tech_text: data?.body?.contents?.tech?.text || '',
  })
  const [loading, setLoading] = useState(false)

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value, name } = e.currentTarget
    setInput({ ...input, [name]: value })
  }

  const onSubumit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    setLoading(true)
    try {
      await set(ref(db, 'about/body/contents'), {
        idn: {
          title: input.idn_title,
          text: input.idn_text,
        },
        eng: {
          title: input.eng_title,
          text: input.eng_text,
        },
        tech: {
          title: input.tech_title,
          text: input.tech_text,
        },
      })
      Swal.fire({
        title: 'Contents Successfully updated',
        icon: 'success',
      })
    } catch (e: any) {
      Swal.fire({
        title: 'Failed to update Contents!',
        text: e.message,
        icon: 'error',
      })
    }
    setLoading(false)
  }

  return (
    <div className="card mb-3">
      <div className="card-header">Contents</div>
      <div className="card-body">
        <Form onSubmit={onSubumit}>
          <h4>INDONESIA</h4>
          <FormGroup>
            <Label for="idn_title">Title</Label>
            <Input id="idn_title" name="idn_title" value={input.idn_title} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Label for="idn_text">Text</Label>
            <Input id="idn_text" name="idn_text" value={input.idn_text} onChange={onChange} />
          </FormGroup>

          <h4>ENGLISH</h4>
          <FormGroup>
            <Label for="eng_title">Title</Label>
            <Input id="eng_title" name="eng_title" value={input.eng_title} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Label for="eng_text">Text</Label>
            <Input id="eng_text" name="eng_text" value={input.eng_text} onChange={onChange} />
          </FormGroup>

          <h4>TECH I LOVE</h4>
          <FormGroup>
            <Label for="tech_title">Title</Label>
            <Input id="tech_title" name="tech_title" value={input.tech_title} onChange={onChange} />
          </FormGroup>
          <FormGroup>
            <Label for="tech_text">Text</Label>
            <Input id="tech_text" name="tech_text" value={input.tech_text} onChange={onChange} />
          </FormGroup>

          <FormGroup>
            <Button type="submit" color="primary" disabled={loading}>
              Submit
            </Button>
          </FormGroup>
        </Form>
      </div>
    </div>
  )
}
