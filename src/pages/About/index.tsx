import { ref, getDatabase } from 'firebase/database'
import { useObjectVal } from 'react-firebase-hooks/database'
import { IAboutPage } from '../../firebase/types'
import FormHead from '../../components/FormHead'
import FormContents from './FormContents'
import ImageUploader from './ImageUploader'
import FormEditImage from './FormEditImage'
import FormBody from './FormBody'

const db = getDatabase()
const headRef = ref(db, 'about/head')

export default function AboutPage() {
  const [value, loading] = useObjectVal<IAboutPage>(ref(db, 'about'))
  if (loading) return <></>
  return (
    <div className="container py-4">
      <FormHead headRef={headRef} head={value?.head} />
      <FormBody data={value} />
      <FormContents data={value} />
      <ImageUploader />
      {value?.body?.image && <FormEditImage image={value?.body?.image} />}
    </div>
  )
}
