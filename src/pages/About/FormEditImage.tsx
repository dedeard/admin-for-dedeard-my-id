import { useState } from 'react'
import { IDataImage } from '../../firebase/types'
import { Input, InputGroup, InputGroupText, Button } from 'reactstrap'
import { getDatabase, ref as dRef, update } from 'firebase/database'
import Swal from '../../utils/swal'

const db = getDatabase()

const FormEditImage = ({ image }: { image: IDataImage }) => {
  const [alt, setAlt] = useState(image.alt)
  const [saveLoading, setSaveLoading] = useState(false)

  const save = async () => {
    setSaveLoading(true)
    const ref = dRef(db, 'about/body/image')
    try {
      await update(ref, { alt })
      Swal.fire({
        title: 'Image ALT Successfully updated!',
        icon: 'success',
      })
    } catch (e: any) {
      Swal.fire({
        title: 'Failed to update Image ALT!',
        text: e.message,
        icon: 'error',
      })
    }
    setSaveLoading(false)
  }

  return (
    <div className="my-3">
      <div className="card">
        <img src={image.sm_url} className="card-img-top" alt={image.alt} />
        <div className="card-body">
          <InputGroup size="sm">
            <InputGroupText>ALT</InputGroupText>
            <Input placeholder="Image ALT" value={alt} onChange={(e) => setAlt(e.currentTarget.value)} />
            {alt !== image.alt && (
              <Button color="primary" onClick={save} disabled={saveLoading}>
                Save
              </Button>
            )}
          </InputGroup>
        </div>
      </div>
    </div>
  )
}

export default FormEditImage
