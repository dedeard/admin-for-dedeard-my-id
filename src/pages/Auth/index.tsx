import { useState } from 'react'
import { getAuth, GoogleAuthProvider, signInWithPopup } from 'firebase/auth'
import { Button } from 'reactstrap'
import Swal from '../../utils/swal'

const auth = getAuth()
export default function AuthPage() {
  const [loading, setLoading] = useState(false)

  const signInWithGoogle = () => {
    setLoading(true)
    signInWithPopup(auth, new GoogleAuthProvider()).catch((e) => {
      Swal.fire({
        title: 'SIGN IN FAILED',
        text: e.message,
        icon: 'error',
      })
      setLoading(false)
    })
  }
  return (
    <div className="py-5 container">
      <h1 className="text-center mb-3">SIGN IN</h1>
      <div className="row">
        <div className="col-md-4 mx-auto">
          <div className="card card-body shadow">
            <Button color="primary" block onClick={signInWithGoogle} disabled={loading}>
              SIGN IN WITH GOOGLE
            </Button>
          </div>
        </div>
      </div>
    </div>
  )
}
