import React, { useEffect, useState } from 'react'
import { ref, getDatabase, set } from 'firebase/database'
import { useObjectVal } from 'react-firebase-hooks/database'
import { Form, Button, FormGroup, Input, Label } from 'reactstrap'
import Swal from '../../utils/swal'

const db = getDatabase()
const configRef = ref(db, 'config')

export default function GalleryPage() {
  const [input, setInput] = useState({ imgbb_key: '' })
  const [loading, setLoading] = useState(false)
  const [value] = useObjectVal<{ imgbb_key: string }>(configRef)

  useEffect(() => {
    if (value?.imgbb_key) setInput({ imgbb_key: value.imgbb_key })
  }, [value])

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value, name } = e.currentTarget
    setInput({ ...input, [name]: value })
  }

  const onSubumit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    setLoading(true)
    try {
      await set(configRef, input)
      Swal.fire({
        title: 'Config Successfully updated',
        icon: 'success',
      })
    } catch (e: any) {
      Swal.fire({
        title: 'Failed to upload config!',
        text: e.message,
        icon: 'error',
      })
    }
    setLoading(false)
  }

  return (
    <div className="container py-4">
      <div className="card mb-3">
        <div className="card-header">Config</div>
        <div className="card-body">
          <Form onSubmit={onSubumit}>
            <FormGroup>
              <Label for="imgbb_key">ImgBB api key</Label>
              <Input id="imgbb_key" name="imgbb_key" value={input.imgbb_key} onChange={onChange} />
            </FormGroup>
            <FormGroup>
              <Button type="submit" color="primary" disabled={loading}>
                Submit
              </Button>
            </FormGroup>
          </Form>
        </div>
      </div>
    </div>
  )
}
