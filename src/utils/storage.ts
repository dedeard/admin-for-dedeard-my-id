import { firebaseConfig } from '../firebase'

export function createStorageUrl(name: string): string {
  return `https://storage.googleapis.com/${firebaseConfig.storageBucket}/${name}`
}

export function normalizeStorageUrl(url: string): string {
  return url.replace(`https://storage.googleapis.com/${firebaseConfig.storageBucket}/`, '')
}
