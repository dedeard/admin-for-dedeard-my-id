import MainSwal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const Swal = withReactContent(MainSwal)

export default Swal
