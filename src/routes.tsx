import { Route } from 'react-router-dom'
import AuthPage from './pages/Auth'
import HomePage from './pages/Home'
import GalleryPage from './pages/Gallery'
import { AuthMiddleware, GuestMiddleware } from './middlewares'
import ContactPage from './pages/Contact'
import AboutPage from './pages/About'
import ConfigPage from './pages/Config'

const routes = (
  <>
    <Route
      path="/"
      element={
        <AuthMiddleware>
          <HomePage />
        </AuthMiddleware>
      }
    />
    <Route
      path="/about"
      element={
        <AuthMiddleware>
          <AboutPage />
        </AuthMiddleware>
      }
    />
    <Route
      path="/gallery"
      element={
        <AuthMiddleware>
          <GalleryPage />
        </AuthMiddleware>
      }
    />
    <Route
      path="/contact"
      element={
        <AuthMiddleware>
          <ContactPage />
        </AuthMiddleware>
      }
    />
    <Route
      path="/config"
      element={
        <AuthMiddleware>
          <ConfigPage />
        </AuthMiddleware>
      }
    />
    <Route
      path="/auth"
      element={
        <GuestMiddleware>
          <AuthPage />
        </GuestMiddleware>
      }
    />
  </>
)

export default routes
