import { Routes } from 'react-router-dom'
import AppNavbar from './components/AppNavbar'
import { useAuthState } from 'react-firebase-hooks/auth'
import { getAuth } from 'firebase/auth'
import routes from './routes'

const auth = getAuth()

export default function App() {
  const [user, loading] = useAuthState(auth)
  if (loading) return <></>
  return (
    <>
      <AppNavbar user={user} />
      <Routes>{routes}</Routes>
    </>
  )
}
