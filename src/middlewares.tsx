import { useLocation, Navigate } from 'react-router-dom'
import { useAuthState } from 'react-firebase-hooks/auth'
import { getAuth } from 'firebase/auth'

const auth = getAuth()

export const AuthMiddleware = ({ children }: { children: JSX.Element }) => {
  let location = useLocation()
  const [user] = useAuthState(auth)
  if (!user) return <Navigate to="/auth" state={{ from: location }} replace />
  return children
}

export const GuestMiddleware = ({ children }: { children: JSX.Element }) => {
  let location = useLocation()
  const [user] = useAuthState(auth)
  if (user) return <Navigate to="/" state={{ from: location }} replace />
  return children
}
