import { initializeApp } from 'firebase/app'

export const firebaseConfig = {
  apiKey: 'AIzaSyCVXW6MTdRVtYPTOoV92ruBQ3ZQcF5Ho0g',
  authDomain: 'dede-ard.firebaseapp.com',
  databaseURL: 'https://dede-ard.firebaseio.com',
  projectId: 'dede-ard',
  storageBucket: 'dede-ard.appspot.com',
  messagingSenderId: '120930847292',
  appId: '1:120930847292:web:5ec90155ed148806b65139',
  measurementId: 'G-ESGCTM4RTL',
}

initializeApp(firebaseConfig)
