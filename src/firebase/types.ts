export interface IDataHead {
  title?: string
  description?: string
}

export interface IImgBBImage {
  extension: string
  filename: 'Qj-OKWFa-D8ci-AUw-E60-Vl-P7c-DZU-jpeg.jpg'
  mime: 'image/jpeg'
  name: 'Qj-OKWFa-D8ci-AUw-E60-Vl-P7c-DZU-jpeg'
  url: 'https://i.ibb.co/bXZmY6t/Qj-OKWFa-D8ci-AUw-E60-Vl-P7c-DZU-jpeg.jpg'
}

export interface IDataImage {
  alt: string
  url: string
  sm_url: string
  fileName?: string
  createdAt?: number
  imgbb?: {
    id: string
    size: number
    image_url: string
    medium_url: string
    thumb_url: string
    delete_url: string
  }
}

export interface IDataBackground {
  image: string
  video: string
}

export interface IHomePage {
  head?: IDataHead
  body?: {
    socials?: {
      [key: string]: string
    }
    contents?: {
      [key: string]: string
    }
  }
}

export interface IAboutPage {
  head?: IDataHead
  body?: {
    title?: string
    resume?: string
    image?: IDataImage
    contents?: {
      [key: string]: {
        title: string
        text: string
      }
    }
  }
}

export interface IGalleryPage {
  head?: IDataHead
  body?: {
    title?: string
    images?: {
      [key: string]: IDataImage
    }
  }
}

export interface IContactPage {
  head?: IDataHead
  body?: {
    title?: string
    formspree_key?: string
    contents?: {
      [key: string]: {
        title: string
        text: string
      }
    }
  }
}
